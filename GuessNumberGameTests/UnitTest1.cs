using Microsoft.VisualStudio.TestTools.UnitTesting;
using Task4.GuessNumberGame;
namespace GuessNumberGameTests
{
    [TestClass]
    public class UnitTest1
    {
       
        [TestMethod]
        public void GetHint_Try_to_take_a_biger_numberreturned()
        {
            //arrange
            int hiddenNumber = 50;
            int value = 40;
            string expected = "Try to take a bigger number";

            //act
            GuessNumberGame game = new Task4.GuessNumberGame.GuessNumberGame(hiddenNumber);
            string actual = game.GetHint(value);

            //assert
            Assert.AreEqual(expected, actual);

        }

        [TestMethod]
        public void GetHint_Try_to_take_a_smaller_numberreturned()
        {
            //arrange
            int hiddenNumber = 40;
            int value = 50;
            string expected = "Try to take a smaller number";

            //act
            GuessNumberGame game = new Task4.GuessNumberGame.GuessNumberGame(hiddenNumber);
            string actual = game.GetHint(value);

            //assert
            Assert.AreEqual(expected, actual);

        }

        [TestMethod]
        public void AreYouWin_CorrectValue_truereturned()
        {
            //arrange
            int hiddeValue = 50;
            int value = 50;
            bool expected = true;

            //act
            GuessNumberGame game = new Task4.GuessNumberGame.GuessNumberGame(hiddeValue);
            bool actual = game.AreYouWin(value);

            //assert
            Assert.AreEqual(expected, actual);

        }

    }
}
